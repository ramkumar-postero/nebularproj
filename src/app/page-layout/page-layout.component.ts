import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-page-layout',
  template: `

  <nb-layout>
    <nb-layout-header fixed>Company Name</nb-layout-header>

    <nb-sidebar>Sidebar Content</nb-sidebar>

    <nb-layout-column>Page Content</nb-layout-column>
  </nb-layout>
`,
  styleUrls: ['./page-layout.component.css']
})
export class PageLayoutComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
